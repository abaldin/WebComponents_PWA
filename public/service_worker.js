(function() {
    'use strict';

    const CACHE_NAME = 'cache_1';
    const resourceList = [
        '/',
        '/navigator.html',
        '/service_worker.js',
        '/fancy-menu/fancy-menu.html',
        '/fancy-menu/fancy-menu.css',
        '/fancy-footer/fancy-footer.html',
        '/fancy-footer/fancy-footer.css'
    ]

    self.addEventListener('install', function(event) {
        console.log('Service worker installing...');

        // extends lifetime of install until the passed promise resolves
        // successuflly. If rejecting, the installation is considered a failure
        event.waitUntil(
            // cache on install
            caches.open(CACHE_NAME).then(function(cache) {
                return cache.addAll(resourceList);
            })
        );

    });

    self.addEventListener('activate', function(event) {
        console.log('Service worker activating...');
    });

    function addToCache(cacheName, request, response) {
        console.log('adding to cache: ', request.url);
        caches.open(cacheName).then(cache => {
            return cache.put(request, response  );
        });
    }

    function updateCache(request) {
        // todo: only update if fetched contents are newer.
        //       reload page after update...
        new Promise(function(resolve, reject) {
            fetch(request).then(function(response) {
                addToCache(CACHE_NAME, request, response);
                resolve(response);
                console.log('cache updated.');
            })
        });
    }

    self.addEventListener('fetch', function(event) {
        // Cache falling back to network
        event.respondWith(
            caches.match(event.request).then(function(response) {
                if (response === undefined) {
                    console.log('fetching from network: ', event.request.url);
                    return fetch(event.request).then(response => {
                        addToCache(CACHE_NAME, event.request, response.clone());
                        return response;
                    });
                } else {
                    console.log('loading from cache: ', event.request.url);
                    //updateCache(event.request);
                    return response;
                }
            })
        );
    });

})();
