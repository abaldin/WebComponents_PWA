## Progressive Web App using W3C Web Components

A small PWA project developed during the Web Engineering course demonstrating the following technologies:

* app-shell
* service worker for offline functionality
* responsive design (viewport meta tag & manifest.json)
* HTML Imports
* HTML Templates
* custom elements
* shadow dom


Try the demo in Google Chrome (http://localhost:3000):

``node index.js``


See the [presentation slides](Presentation.pdf) for more info.