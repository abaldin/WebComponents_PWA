const express = require('express');
const app = express();

//use static middleware for static files
app.use(express.static('public'));

app.get('/', (req, res) => {
	res.sendFile('public/navigator.html', {root: __dirname});
});

app.listen(3000);
console.log('serving at http://localhost:3000');
